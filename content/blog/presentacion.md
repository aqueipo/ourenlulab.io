+++
title = "Presentacion"
date = "2018-01-01T13:39:46+02:00"
tags = ["ourenlu","redes sociales"]
categories = ["presentacion"]
banner = "img/carousel/DSC_2167.jpg"
+++

## Presentación en redes sociales

Aunque Ourenlú Quartet cuenta ya con un año de recorrido como formación musical que ameniza eventos de cualquier tipo, hemos querido esperar al recién estrenado 2018 para, con más experiencia y convencimiento, darnos a conocer en las redes sociales. Un nuevo año lleva parejo nuevos proyectos y os aseguramos que afrontamos este nuevo reto con muchas ganas e ilusión.
Hemos pedido a los Reyes Magos una página web para dar visibilidad a nuestro grupo y, en manos de su page, nos la han concedido. A través de esta página sabréis quiénes somos, qué hacemos, dónde actuamos y por qué elegirnos. Mediante ella, además, podréis contactarnos: por teléfono, si queréis hacerlo directamente, o por Email, para el envío/recepción de correos electrónicos. También tendréis acceso a nuestra cuenta de Facebook, para hacernos llegar vuestros comentarios o ver nuestras publicaciones, a Instagram, para visualizar nuestras fotos, al canal de Ourenlú en YouTube, si deseáis escuchar y ver nuestros vídeos, y a nuestro blog donde irán apareciendo las noticias más relevantes sobre nuestras actuaciones.
De antemano, gracias a todos por seguirnos y por vuestra confianza. Y especialmente a ti, Adri, no sólo por crear y regalarnos este espacio que nos encanta, sino también por compartir con nosotros esta nueva ilusión

