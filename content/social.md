+++
title = "Social"
description = "Todo lo relacionado con Ourenlu Quartet"
keywords = ["Social","redes","facebook", "videos"]
+++

<div class="row">
    <div class="col-md-4">
    <!-- lista videos youtube -->
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/I40ARu8rXPI" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/dmN_XR5TOlQ" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/KPeRDu69ONY" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/z61vTPSu-7E" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/lZH_ck_edvU" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/vex0WdTvcI0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/4SjOcfK8TaE" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
        <ul>
            <iframe width="280" height="157" src="https://www.youtube.com/embed/PvEBO9th6Yg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </ul>
    </div>
    <div class="col-md-4">
        <!-- feed twitter -->
        <a class="twitter-timeline" href="https://twitter.com/ourenluquartet?ref_src=twsrc%5Etfw">Tweets by ourenluquartet</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        <!-- feed instagram a traves de snapwidget -->
        <iframe src="https://snapwidget.com/embed/490244" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:300px; height:300px"></iframe>
    </div>
    <div class="col-md-3" style="float:right;">
        <!-- feed facebook -->
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fourenluquartet&locale=es_ES&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden;float:right;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
    </div>
</div>
